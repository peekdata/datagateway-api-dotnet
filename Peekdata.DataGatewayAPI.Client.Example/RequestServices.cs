﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Peekdata.DataGatewayAPI.Client.Model;



namespace Peekdata.DataGatewayAPI.Client.Example
{
    /// <summary>
    ///  Class used as list of Examples how Requests can be formed
    /// </summary>
    public static class RequestServices
    {

        // <copyright file="RequestServices" company="Peekdata.io">
        // Copyright by Peekdata.io (c) 2017-2020 All Rights Reserved	  
        // <author>Dionizas Antipenkovas, d.antipenkovas@peedata.io</author>
        // </copyright>

        /// <summary>
        ///  Example of Request containing 2 Dimensions, 2 Metrics, 1 Filter and 1 Sorting option
        /// </summary>
        /// <returns></returns>
        public static GetDataRequest getTwoDimensionsTwoMetricsFilterAndSorting()
        {
            GetDataRequest request = new GetDataRequest();

            request.scopeName = "Mortgage-Lending";

            request.dimensions = new string[2] { "propertyCityID", "currency" };

            request.metrics = new string[2] { "loanamount", "totalincome" };

            request.filters = new FilterDto();
            request.filters.singleKeys = new List<SingleKeyFilterDto>();

            request.filters.singleKeys.Add(new SingleKeyFilterDto("currency", FilterOperation.EQUALS, new string[] { "EUR" }));

            request.sortings = new SortDto();
            request.sortings.dimensions = new SortKeyDto[1];
            request.sortings.dimensions[0] = new SortKeyDto("currency", SortDirection.ASC);

            return request;
        }

        /// <summary>
        /// Example of Request containing 1 dimension, 2 Metrics, 2 Filters from Specified Data Model (Data Source)
        /// </summary>
        /// <returns></returns>
        public static GetDataRequest getOneDimensionTwoMetricsAndTwoFilter()
        {
            GetDataRequest request = new GetDataRequest();

            request.scopeName = "Mortgage-Lending";

            request.dimensions = new string[1] { "currency" };

            request.metrics = new string[2] { "loanamount", "waintrate" };

            request.filters = new FilterDto();
            request.filters.dateRanges = new List<DateRangeFilterDto>();
            request.filters.dateRanges.Add(new DateRangeFilterDto("closingdate","2017-01-01","2017-12-31"));

            request.filters.singleKeys = new List<SingleKeyFilterDto>();
            request.filters.singleKeys.Add(new SingleKeyFilterDto("officerid", FilterOperation.EQUALS, new string[] { "1", "2", "3" }));
             
            return request;
        }
    }
}
