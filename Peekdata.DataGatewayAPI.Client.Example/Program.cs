﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;

using Peekdata.DataGatewayAPI.Client;
using Peekdata.DataGatewayAPI.Client.Model;


namespace Peekdata.DataGatewayAPI.Client.Example
{

    class Program
    {

        // <copyright file="Program" company="Peekdata.io">
        // Copyright by Peekdata.io (c) 2017-2020 All Rights Reserved	  
        // <author>Dionizas Antipenkovas, d.antipenkovas@peedata.io</author>
        // </copyright>

        static void Main(string[] args)
        {
            // run examples
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            // initialize client
            APIServices api = new APIServices("https://demo.peekdata.io:8443"); //             

            try { 

                Stopwatch sw = Stopwatch.StartNew();

                // check service
                bool isOk = await api.Helthcheck();

                sw.Stop();

                if (isOk) {
                    Console.WriteLine("Healthcheck ms:{0}", sw.ElapsedMilliseconds);
                }
                else
                {
                    Console.WriteLine("Serice is not available. Press <Enter> to continue...");
                    Console.ReadLine();
                    return;
                }
            
                // get request
                GetDataRequest request = RequestServices.getTwoDimensionsTwoMetricsFilterAndSorting();

                sw.Restart();
                string select = await api.GetQuery(request);

                Console.WriteLine(select);

                sw.Stop();
                Console.WriteLine("Got Query(SELECT) statement in ms:{0}", sw.ElapsedMilliseconds);

                sw.Restart();
                GetDataOptimizedResponse oreport = await api.GetDataOptimized(request);

                sw.Stop();
                Console.WriteLine("Got Optimized DATA in ms:{0}", sw.ElapsedMilliseconds);

                /// Print out data
                if (oreport != null)
                {
                    Console.Write("Column Headers:");
                    foreach (ColumnHeader s in oreport.columnHeaders)
                        Console.Write("{0},", s.name);
                    Console.WriteLine();

                    if (oreport.rows != null)
                    {
                        // print out Column headers
                        foreach (object[] r in oreport.rows)
                        {
                            foreach (object o in r)
                                Console.Write("{0},", o.ToString());
                            Console.WriteLine();
                        }
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.Write("Empty result set.");
                    }
                }

                sw.Restart();
                GetDataResponse report = await api.GetData(request);

                sw.Stop();
                Console.WriteLine("Got DATA in ms:{0}", sw.ElapsedMilliseconds);

                /// Print out data
                if (report != null)
                {
                    Console.Write("Column Headers:");
                    foreach(ColumnHeader s in report.columnHeaders)
                        Console.Write("{0},", s.name);
                    Console.WriteLine();

                    if (report.rows != null)
                    {
                        // print out Column headers
                        foreach(RowDataDto r in report.rows)
                        {
                            foreach (KeyValuePair<string, object> o in r.vals)
                                Console.Write("{0},", o.Value.ToString());
                            Console.WriteLine();
                        }
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.Write("Empty result set.");
                    }
                }            

                sw.Restart();

                bool fileCreated = await api.GetCSV(request, @"c:\temp\test.csv");

                sw.Stop();

                Console.WriteLine("Got FILE in ms:{0}", sw.ElapsedMilliseconds);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("End of program, press <Enter> to continue...");
            Console.ReadLine();
        }
    }
}
