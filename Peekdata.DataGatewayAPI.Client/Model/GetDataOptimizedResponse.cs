﻿using Peekdata.DataGatewayAPI.Client.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peekdata.DataGatewayAPI.Client.Model
{
    public class GetDataOptimizedResponse
    {
        public string requestID;
        public List<ColumnHeader> columnHeaders;
        public List<object[]> rows;
        public int totalRows = 0;

        public GetDataOptimizedResponse()
        {
        }

        public GetDataOptimizedResponse(string requestID)
        {
            this.requestID = requestID;
        }

        public GetDataOptimizedResponse(string requestID, List<ColumnHeader> columnHeaders, List<object[]> rows, int totalRows)
        {
            this.requestID = requestID;
            this.columnHeaders = columnHeaders;
            this.rows = rows;
            this.totalRows = totalRows;
        }
    }
}
