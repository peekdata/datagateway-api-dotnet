﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peekdata.DataGatewayAPI.Client.Model
{
    public class DataRequestOptionDto
    {
        public Dictionary<string, string> rows = new Dictionary<string, string>();
        public Dictionary<string, string> arguments = new Dictionary<string, string>();
    }
}
