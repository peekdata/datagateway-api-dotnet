﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peekdata.DataGatewayAPI.Client.Model
{
    public class DateRangeFilterDto
    {
        public string key;
        public string from;
        public string to;

        public DateRangeFilterDto()
        {
        }

        public DateRangeFilterDto(string from, string to)
        {
            this.from = from;
            this.to = to;
        }

        public DateRangeFilterDto(string key, string from, string to)
        {
            this.key = key;
            this.from = from;
            this.to = to;
        }
    }
}
