﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peekdata.DataGatewayAPI.Client.Model
{
    public class SortDto
    {
        public SortKeyDto[] dimensions;
        public SortKeyDto[] metric;
    }

}
