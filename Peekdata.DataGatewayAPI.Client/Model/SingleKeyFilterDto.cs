﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peekdata.DataGatewayAPI.Client.Model
{
    public class SingleKeyFilterDto
    {
        public String key;
        public FilterOperation operation;
        public string[] values;

        public SingleKeyFilterDto()
        {

        }

        public SingleKeyFilterDto(String key, FilterOperation operation, string[] values)
        {
            this.key = key;
            this.operation = operation;
            this.values = values;
        }
    }
}
