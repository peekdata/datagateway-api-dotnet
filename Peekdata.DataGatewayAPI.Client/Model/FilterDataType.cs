﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peekdata.DataGatewayAPI.Client.Model
{
    public enum FilterDataType{
        AUTO,
        NUMBER,
        DATE,
        STRING,
        CHAR
    }
}
