﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peekdata.DataGatewayAPI.Client.Model
{
    public class SortKeyDto
    {
        public String key;
        public SortDirection direction = SortDirection.ASC;

        public SortKeyDto()
        {
        }

        public SortKeyDto(String key, SortDirection direction)
        {
            this.key = key;
            this.direction = direction;
        }

        public SortKeyDto(String key) : this(key, SortDirection.ASC)
        {
            
        }       
    }
}
