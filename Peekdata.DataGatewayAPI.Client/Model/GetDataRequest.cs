﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peekdata.DataGatewayAPI.Client.Model
{
    /// <summary>
    /// The main class used to pass parameters to API.
    /// </summary>
    public class GetDataRequest
    {
        /// <summary>
        /// ID in a form of GUID of request used to track request through logs. Can be empty, then system will generate it 
        /// </summary>
        public string requestID;
        /// <summary>
        /// Information about caller, used for logging
        /// </summary>
        string consumerInfo;
        /// <summary>
        /// Name of Business domain. DataModel are grouped by Scope. Required
        /// </summary>
        public string scopeName;
        /// <summary>
        /// Name of Datasource. If not speficied, DataGateway Engine selects on his owned based on priorities and compliance to the request 
        /// </summary>
        public string dataModel;
        /// <summary>
        /// List of dimensions.
        /// </summary>
        public string[] dimensions;
        /// <summary>
        /// List of Metrics. At least one metric should be definded in the call
        /// </summary>
        public string[] metrics;
        /// <summary>
        /// Composite class of filteting options
        /// </summary>
        public FilterDto filters;
        /// <summary>
        /// Sorting options
        /// </summary>
        public SortDto sortings;
        /// <summary>
        /// DataRequest options
        /// </summary>
        public DataRequestOptionDto options;
    }
}
