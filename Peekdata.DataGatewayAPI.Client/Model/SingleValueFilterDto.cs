﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peekdata.DataGatewayAPI.Client.Model
{
    /**
    *
    * @author d.antipenkovas
    */
    public class SingleValueFilterDto
    {
        public String[] keys;
        public FilterOperation operation;
        public String value;
        public FilterDataType type = FilterDataType.AUTO; // used for Quouting

        public SingleValueFilterDto()
        {
        }

        public SingleValueFilterDto(string[] keys, String value)
        {
            this.keys = keys;
            this.operation = FilterOperation.EQUALS;
            this.value = value;
        }

        public SingleValueFilterDto(string[] keys, FilterOperation operation, string value)
        {
            this.keys = keys;
            this.operation = operation;
            this.value = value;
        }

        public SingleValueFilterDto(string[] keys, FilterOperation operation, string value, FilterDataType type)
        {
            this.keys = keys;
            this.operation = operation;
            this.value = value;
            this.type = type;
        }
    }
}
