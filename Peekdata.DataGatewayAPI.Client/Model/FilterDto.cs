﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peekdata.DataGatewayAPI.Client.Model
{
    public class FilterDto
    {
        public List<DateRangeFilterDto> dateRanges;

        public List<SingleKeyFilterDto> singleKeys;

        public List<SingleValueFilterDto> singleValues;

    }
}
