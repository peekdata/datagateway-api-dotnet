﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peekdata.DataGatewayAPI.Client.Model
{
    public enum FilterOperation {
        EQUALS,
        NOT_EQUALS,
        STARTS_WITH,
        NOT_STARTS_WITH,
        ALL_IS_LESS,
        ALL_IS_MORE,
        AT_LEAST_ONE_IS_LESS,
        AT_LEAST_ONE_IS_MORE
    }
}
