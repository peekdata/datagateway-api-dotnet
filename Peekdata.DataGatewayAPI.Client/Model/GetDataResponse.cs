﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peekdata.DataGatewayAPI.Client.Model
{
    public class GetDataResponse
    {
        public String requestID;
        public List<ColumnHeader> columnHeaders;
        public List<RowDataDto> rows;
        public int totalRows = 0;
    }
}
