﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peekdata.DataGatewayAPI.Client.Model
{
    public class ColumnHeader
    {
        public string title;
        public string name;
        public string dataType;
        public string format;
        public string alias;

        public ColumnHeader()
        {
            // default constructor
        }
    }
}
