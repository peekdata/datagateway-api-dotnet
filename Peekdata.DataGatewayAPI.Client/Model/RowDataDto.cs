﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peekdata.DataGatewayAPI.Client.Model
{
    [JsonConverter(typeof(RowDataConverter))]
    public class RowDataDto
    {
        public Dictionary<string, object> vals = new Dictionary<string, object>();
        public RowDataDto()
        {
        }
    }
}
