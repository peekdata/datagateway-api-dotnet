﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using Peekdata.DataGatewayAPI.Client.Model;
using System.Web.Script.Serialization;

/// <summary>
/// Examples of how to form calls of Peekdata DataGateway API
/// Pre-requisites: uses Microsoft.AspNet.WebApi.Client nuget package
/// </summary>

namespace Peekdata.DataGatewayAPI.Client
{
    public class APIServices
    {

        // <copyright file="APIServices" company="Peekdata.io">
        // Copyright by Peekdata.io (c) 2017-2020 All Rights Reserved	  
        // <author>Dionizas Antipenkovas, d.antipenkovas@peedata.io</author>
        // </copyright>

        private HttpClient client = new HttpClient();

        private const String CONST_API_METHOD_HEALTHCHECK = "/datagateway/rest/v1/healthcheck";
        private const String CONST_API_METHOD_GETQUERY    = "/datagateway/rest/v1/query/get";
        private const String CONST_API_METHOD_GETDATA = "/datagateway/rest/v1/data/get";
        private const String CONST_API_METHOD_GETDATA_OPTIMIZED = "/datagateway/rest/v1/data/getoptimized";
        private const String CONST_API_METHOD_GETCSV = "/datagateway/rest/v1/file/get";

        // default constructor
        public APIServices(string url)
        {
            this.client.BaseAddress = new Uri(url);
            this.client.DefaultRequestHeaders.Accept.Clear();
            this.client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            // Make sure HTTPS works correctly
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
        }

        /// <summary>
        /// Example of GET Helthcheck method call
        /// </summary>
        /// <returns>True if Helthcheck call has been successfull</returns>
        public async Task<bool> Helthcheck()
        {
            HttpResponseMessage response = await this.client.GetAsync(CONST_API_METHOD_HEALTHCHECK);
            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Example of
        /// </summary>
        /// <param name="request"></param>
        /// <returns>SELECT statement</returns>
        public async Task<string> GetQuery(GetDataRequest request)
        {
            string report = null;
            HttpResponseMessage response = await this.client.PostAsJsonAsync(CONST_API_METHOD_GETQUERY, request);

            if (response.IsSuccessStatusCode)
            {
                report = await response.Content.ReadAsStringAsync();
            }
            return report;
        }

        /// <summary>
        /// Returns data in JSON format.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<GetDataResponse> GetData(GetDataRequest request)
        {
            GetDataResponse report = null;
            HttpResponseMessage response = await this.client.PostAsJsonAsync(CONST_API_METHOD_GETDATA, request);

            if (response.IsSuccessStatusCode)
            {
                report = await response.Content.ReadAsAsync<GetDataResponse>();
            }
            return report;
        }

        /// <summary>
        /// Returns data in JSON format.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<GetDataOptimizedResponse> GetDataOptimized(GetDataRequest request)
        {
            GetDataOptimizedResponse report = null;
            HttpResponseMessage response = await this.client.PostAsJsonAsync(CONST_API_METHOD_GETDATA_OPTIMIZED, request);

            if (response.IsSuccessStatusCode)
            {
                report = await response.Content.ReadAsAsync<GetDataOptimizedResponse>();
            }
            return report;
        }

        /// <summary>
        /// Saves data to CSV file
        /// </summary>
        /// <param name="request"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public async Task<bool> GetCSV(GetDataRequest request, string fileName)
        {
            HttpResponseMessage response = await this.client.PostAsJsonAsync(CONST_API_METHOD_GETCSV, request);

            if (response.IsSuccessStatusCode)
            {
                byte[] data = await response.Content.ReadAsByteArrayAsync();
                File.WriteAllBytes(fileName, data);
                return (true);               
            }

            return false;
        }

    }
}
